package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int side = calcHeight(inputNumbers.size());
        if (side <= 0) {
            throw new CannotBuildPyramidException();
        }

        try {
            Collections.sort(inputNumbers);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }

        int[][] pyramid = new int[side][side * 2 - 1];
        Iterator<Integer> it = inputNumbers.iterator();

        for (int i = 1; i <= side; ++i) {
            int startPos = side - i;
            for (int j = 0; j < i; ++j) {
                pyramid[i - 1][startPos + j * 2] = it.next();
            }
        }

        return pyramid;
    }

    private int calcHeight(int listSize) {
        long doubleSize = listSize * 2L;

        int n = 1;
        while (n * (n + 1L) < doubleSize) {
            n += 1;
        }

        if (n * (n + 1L) == doubleSize) {
            return n;
        } else {
            return 0;
        }
    }

}
