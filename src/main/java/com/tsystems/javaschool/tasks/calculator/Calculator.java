package com.tsystems.javaschool.tasks.calculator;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class Calculator {
    private static final Character ADD = '+';
    private static final Character SUBTRACT = '-';
    private static final Character MULTIPLY = '*';
    private static final Character DIVIDE = '/';

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }

        try {
            Reader in = new StringReader(statement);
            double num = parse(in, -1);
            if (!Double.isFinite(num))
                return null;
            String res = String.valueOf(num);
            if (res.endsWith(".0"))
                return res.substring(0, res.length() - 2);
            else
                return res;
        } catch (Exception e) {
            return null;
        }
    }

    private static double parse(Reader in, int end) throws IOException {
        StringBuilder sb = new StringBuilder();
        List<Object> expr = new ArrayList<>();
        for (int ch = in.read(); ch != end; ch = in.read()) {
            if (!isValid(ch)) {
                throw new IOException();
            }

            if (ch == '(') {
                expr.add(parse(in, ')'));
            } else if (isOperator(ch)) {
                if (sb.toString().length() > 0) {
                    expr.add(Double.parseDouble(sb.toString()));
                    sb = new StringBuilder();
                }
                expr.add((char) ch);
            } else {
                sb.append(Character.toChars(ch));
            }
        }
        if (sb.toString().length() > 0) {
            expr.add(Double.parseDouble(sb.toString()));
        }
        return eval(expr);
    }

    private static double eval(List expr) {
        int index;

        if ((index = expr.indexOf(ADD)) >= 0) {
            double a = eval(expr.subList(0, index));
            double b = eval(expr.subList(index + 1, expr.size()));
            return a + b;
        }

        if ((index = expr.indexOf(SUBTRACT)) >= 0) {
            double a = eval(expr.subList(0, index));
            double b = eval(expr.subList(index + 1, expr.size()));
            return a - b;
        }

        if ((index = expr.indexOf('*')) >= 0) {
            double a = eval(expr.subList(0, index));
            double b = eval(expr.subList(index + 1, expr.size()));
            return a * b;
        }

        if ((index = expr.indexOf('/')) >= 0) {
            double a = eval(expr.subList(0, index));
            double b = eval(expr.subList(index + 1, expr.size()));
            return a / b;
        }

        if (expr.size() != 1) {
            throw new IllegalArgumentException();
        }
        return (double) expr.get(0);
    }

    private static boolean isNumber(int ch) {
        return ch == '0' || ch == '1' || ch == '2' || ch == '3' || ch == '4' || ch == '5' || ch == '6' || ch == '7' ||
                ch == '8' || ch == '9' || ch == '.';
    }

    private static boolean isOperator(int ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/';
    }

    private static boolean isValid(int ch) {
        return isNumber(ch) || isOperator(ch) || ch == '(';
    }

}